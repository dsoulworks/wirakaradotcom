$(document).ready(function(){

	$('.readmore').toggle(function(){
		$(this)
			.parent()
			.parent()
			.find('.more')
			//.slideDown('fast');
			.fadeIn();

		$(this).text('Hide Lyrics');

	}, function(){
		$(this)
		.parent()
		.parent()
		.find('.more')
		//.slideUp('fast');
		.fadeOut();

		$(this).text('Show Lyrics');
	})
});