﻿package  {
	import flash.utils.Timer;
	import flash.display.MovieClip;
	import flash.events.TimerEvent;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.display.Shape;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	
	public class Main extends MovieClip{
		
		public var showTitle:Boolean =false;
		public var showBG:Boolean =false;
		public var showMap:Boolean =false;
		public var showSpot:Boolean =false;
		public var showAuck:Boolean =false;
		public var showWell:Boolean =false;
		public var showChrist:Boolean =false;
		public var showSky:Boolean =false;
		public var showPar:Boolean =false;
		public var showBee:Boolean =false;
		public var showCath:Boolean =false;
		public var showChal:Boolean =false;
		public var showAll:Boolean =false;
		
		public var Title:MovieClip = new mcTitle();
		public var startBtn:MovieClip = new mcStartBtn();
		public var credits:MovieClip = new mcCredits();
		public var BG:MovieClip = new mcBlueSky();
		public var map:MovieClip = new mcNZLand();
		public var auckSpot:MovieClip = new mcSpot();
		public var christSpot:MovieClip = new mcSpot();
		public var wellSpot:MovieClip = new mcSpot();
		public var auck:MovieClip = new mcAuck();
		public var christ:MovieClip = new mcChrist();
		public var well:MovieClip = new mcWell();
		public var mapBtn:MovieClip = new mcMapBtn();
		public var nameTxt:MovieClip = new mcName();
		public var backBtn:MovieClip = new mcBackBtn();
		public var photoBtn:MovieClip = new mcPhotoBtn();
		public var lock:MovieClip = new mcLock();
		public var loopBtn:MovieClip = new mcLoopBtn();
		public var all:MovieClip = new mcAllCities();
		public var shot:MovieClip = new mcShot();
		public var photo:MovieClip = new mcPhoto();
		public var photoLabel:MovieClip = new mcPhotoLabel();
		public var LBtn:MovieClip = new mcLBtn();
		public var RBtn:MovieClip = new mcRBtn();
		public var skyLBtn:MovieClip = new mcLBtn();
		public var skyRBtn:MovieClip = new mcRBtn();
		public var parLBtn:MovieClip = new mcLBtn();
		public var parRBtn:MovieClip = new mcRBtn();
		public var beeLBtn:MovieClip = new mcLBtn();
		public var beeRBtn:MovieClip = new mcRBtn();
		public var cathLBtn:MovieClip = new mcLBtn();
		public var cathRBtn:MovieClip = new mcRBtn();
		public var chalLBtn:MovieClip = new mcLBtn();
		public var chalRBtn:MovieClip = new mcRBtn();
		public var skyTower:MovieClip = new mcSkyTower();
		public var parliament:MovieClip = new mcParliament();
		public var beehive:MovieClip = new mcBeehive();
		public var cathedral:MovieClip = new mcCathedral();
		public var chalice:MovieClip = new mcChalice();
		public var phoToMapBtn:MovieClip = new mcBackBtn();
		public var allToMapBtn:MovieClip = new mcBackBtn();
		
		//public var openMusic:GameSound2 = new GameSound2('music/sweet.mp3', 1);
		//public var mainMusic:GameSound2 = new GameSound2('music/kenangan_music.mp3', 1);
		public var blipSound:GameSound = new GameSound('music/blip.mp3', 0.1);
		public var shotSound:GameSound = new GameSound('music/camerashot.mp3', 1);
		
		public var openMusic:Sound = new sweet();
		public var myChannel1:SoundChannel = new SoundChannel();
		
		public var mainMusic:Sound = new kenangan();
		public var myChannel2:SoundChannel = new SoundChannel();

		public function Main() {
// constructor code
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function init(e:Event){
			
			//openMusic.play();
			myChannel1 =openMusic.play(0,int.MAX_VALUE);
			
// add MCs to stage
			addChild(BG);
			BG.x = 360;
			BG.y = 288;
			BG.gotoAndStop(1);
			BG.visible = false;
			
			addChild(map);
			map.x = 360;
			map.y = 288;
			map.gotoAndStop(1);
			map.visible = false;
			
			addChild(auckSpot);
			auckSpot.x = 440;
			auckSpot.y = 131;
			auckSpot.visible = false;
			
			addChild(wellSpot);
			wellSpot.x = 450;
			wellSpot.y = 306;
			wellSpot.visible = false;
			
			addChild(christSpot);
			christSpot.x = 365;
			christSpot.y = 392;
			christSpot.visible = false;
			
			addChild(photo);
			photo.x = 360;
			photo.y = 288;
			photo.gotoAndStop(1);
			photo.visible = false;
			
			addChild(photoLabel);
			photoLabel.x = 360;
			photoLabel.y = 70;
			photoLabel.visible = false;
			
			addChild(skyTower);
			skyTower.x = 360;
			skyTower.y = 285;
			skyTower.gotoAndStop(1);
			skyTower.visible = false;
			
			addChild(parliament);
			parliament.x = 360;
			parliament.y = 285;
			parliament.gotoAndStop(1);
			parliament.visible = false;
			
			addChild(beehive);
			beehive.x = 360;
			beehive.y = 285;
			beehive.gotoAndStop(1);
			beehive.visible = false;
			
			addChild(cathedral);
			cathedral.x = 360;
			cathedral.y = 285;
			cathedral.gotoAndStop(1);
			cathedral.visible = false;
			
			addChild(chalice);
			chalice.x = 360;
			chalice.y = 285;
			chalice.gotoAndStop(1);
			chalice.visible = false;
			
			addChild(auck);
			auck.x = 360;
			auck.y = 285;
			auck.gotoAndStop(1);
			auck.visible = false;
			
			addChild(christ);
			christ.x = 360;
			christ.y = 285;
			christ.gotoAndStop(1);
			christ.visible = false;
			
			addChild(well);
			well.x = 360;
			well.y = 285;
			well.gotoAndStop(1);
			well.visible = false;
			
			addChild(all);
			all.x = 360;
			all.y = 285;
			all.gotoAndStop(1);
			all.visible = false;
			
			addChild(shot);
			shot.x = 360;
			shot.y = 288;
			shot.gotoAndStop(1);
			shot.visible = false;
			
			addChild(Title);
			Title.x = 360;
			Title.y = 288;
			
			addChild(credits);
			credits.x = 360;
			credits.y = 380;
			credits.visible = false;
			
			addChild(startBtn);
			startBtn.x = 360;
			startBtn.y = 320;
			startBtn.visible = false;
			
			addChild(nameTxt);
			nameTxt.x = 105;
			nameTxt.y = 52;
			nameTxt.visible = false;
			
			addChild(backBtn);
			backBtn.x = 500;
			backBtn.y = 540;
			backBtn.visible = false;
			
			addChild(photoBtn);
			photoBtn.x = 570;
			photoBtn.y = 540;
			photoBtn.visible = false;
			
			addChild(lock);
			lock.x = 640;
			lock.y = 540;
			lock.visible = false;
			
			addChild(loopBtn);
			loopBtn.x = 640;
			loopBtn.y = 540;
			loopBtn.gotoAndStop(1);
			loopBtn.visible = false;
			
			addChild(LBtn);
			LBtn.x = 280;
			LBtn.y = 540;
			LBtn.visible = false;
			
			addChild(RBtn);
			RBtn.x = 440;
			RBtn.y = 540;
			RBtn.visible = false;
			
			addChild(skyLBtn);
			skyLBtn.x = 280;
			skyLBtn.y = 540;
			skyLBtn.visible = false;
			
			addChild(skyRBtn);
			skyRBtn.x = 440;
			skyRBtn.y = 540;
			skyRBtn.visible = false;
			
			addChild(parLBtn);
			parLBtn.x = 280;
			parLBtn.y = 540;
			parLBtn.visible = false;
			
			addChild(parRBtn);
			parRBtn.x = 440;
			parRBtn.y = 540;
			parRBtn.visible = false;
			
			addChild(beeLBtn);
			beeLBtn.x = 280;
			beeLBtn.y = 540;
			beeLBtn.visible = false;
			
			addChild(beeRBtn);
			beeRBtn.x = 440;
			beeRBtn.y = 540;
			beeRBtn.visible = false;
			
			addChild(cathLBtn);
			cathLBtn.x = 280;
			cathLBtn.y = 540;
			cathLBtn.visible = false;
			
			addChild(cathRBtn);
			cathRBtn.x = 440;
			cathRBtn.y = 540;
			cathRBtn.visible = false;
			
			addChild(chalLBtn);
			chalLBtn.x = 280;
			chalLBtn.y = 540;
			chalLBtn.visible = false;
			
			addChild(chalRBtn);
			chalRBtn.x = 440;
			chalRBtn.y = 540;
			chalRBtn.visible = false;
			
			addChild(mapBtn);
			mapBtn.x = 360;
			mapBtn.y = 30;
			mapBtn.visible = false;
			
			addChild(phoToMapBtn);
			phoToMapBtn.x = 640;
			phoToMapBtn.y = 540;
			phoToMapBtn.visible = false;
			
			addChild(allToMapBtn);
			allToMapBtn.x = 640;
			allToMapBtn.y = 540;
			allToMapBtn.visible = false;
			
// button mode
			startBtn.buttonMode = true;
			mapBtn.buttonMode = true;
			backBtn.buttonMode = true;
			auckSpot.buttonMode = true;
			wellSpot.buttonMode = true;
			christSpot.buttonMode = true;
			photoBtn.buttonMode = true;
			loopBtn.buttonMode = true;
			skyLBtn.buttonMode = true;
			skyRBtn.buttonMode = true;
			parLBtn.buttonMode = true;
			parRBtn.buttonMode = true;
			beeLBtn.buttonMode = true;
			beeRBtn.buttonMode = true;
			cathLBtn.buttonMode = true;
			cathRBtn.buttonMode = true;
			chalLBtn.buttonMode = true;
			chalRBtn.buttonMode = true;
			phoToMapBtn.buttonMode = true;
			allToMapBtn.buttonMode = true;
			
// mouse events
			startBtn.addEventListener(MouseEvent.CLICK, enterMap1);
			mapBtn.addEventListener(MouseEvent.CLICK, enterMap2);
			backBtn.addEventListener(MouseEvent.CLICK, mainTitle);
			photoBtn.addEventListener(MouseEvent.CLICK, photoAlbum);
			loopBtn.addEventListener(MouseEvent.CLICK, allCities);
			skyLBtn.addEventListener(MouseEvent.CLICK, goToChalB);
			skyRBtn.addEventListener(MouseEvent.CLICK, goToParF);
			parLBtn.addEventListener(MouseEvent.CLICK, goToSkyB);
			parRBtn.addEventListener(MouseEvent.CLICK, goToBeeF);
			beeLBtn.addEventListener(MouseEvent.CLICK, goToParB);
			beeRBtn.addEventListener(MouseEvent.CLICK, goToCathF);
			cathLBtn.addEventListener(MouseEvent.CLICK, goToBeeB);
			cathRBtn.addEventListener(MouseEvent.CLICK, goToChalF);
			chalLBtn.addEventListener(MouseEvent.CLICK, goToCathB);
			chalRBtn.addEventListener(MouseEvent.CLICK, goToSkyF);
			phoToMapBtn.addEventListener(MouseEvent.CLICK, phoToMap);
			allToMapBtn.addEventListener(MouseEvent.CLICK, allToMap);
			
			auckSpot.addEventListener(MouseEvent.CLICK, auckStage);
			wellSpot.addEventListener(MouseEvent.CLICK, wellStage);
			christSpot.addEventListener(MouseEvent.CLICK, christStage);
			
			auckSpot.addEventListener(MouseEvent.ROLL_OVER, auckTxt);
			wellSpot.addEventListener(MouseEvent.ROLL_OVER, wellTxt);
			christSpot.addEventListener(MouseEvent.ROLL_OVER, christTxt);
			credits.addEventListener(MouseEvent.ROLL_OVER, creditsOver);
			
			auckSpot.addEventListener(MouseEvent.ROLL_OUT, NZTxt);
			wellSpot.addEventListener(MouseEvent.ROLL_OUT, NZTxt);
			christSpot.addEventListener(MouseEvent.ROLL_OUT, NZTxt);
			credits.addEventListener(MouseEvent.ROLL_OUT, creditsOut);
			
// main event
			stage.addEventListener(Event.ENTER_FRAME,playGame);
		}
		
// mouse functions
		public function enterMap1(e:MouseEvent):void{
			blipSound.play();
			//openMusic.stop();
			myChannel1.stop();
			//mainMusic.play();
			myChannel2 = mainMusic.play(0,int.MAX_VALUE);
			startBtn.visible = false;
			credits.visible = false;
			Title.gotoAndPlay(41);
		}
		
		public function enterMap2(e:MouseEvent):void{
			blipSound.play();
			mapBtn.visible = false;
			if (auck.currentFrame == 440){
				auck.gotoAndPlay(441);
			}
			if (well.currentFrame == 440){
				well.gotoAndPlay(441);
			}
			if (christ.currentFrame == 440){
				christ.gotoAndPlay(441);
			}
		}
		
		public function mainTitle(e:MouseEvent):void{
			blipSound.play();
			//mainMusic.stop();
			//openMusic.play();
			myChannel2.stop();
			myChannel1 = openMusic.play(0,int.MAX_VALUE);
			backBtn.visible = false;
			photoBtn.visible = false;
			lock.visible = false;
			loopBtn.gotoAndStop(1);
			loopBtn.visible = false;
			auckSpot.visible = false;
			wellSpot.visible = false;
			christSpot.visible = false;
			nameTxt.visible = false;
			map.gotoAndPlay(62);
			BG.gotoAndPlay(21);
		}
		
		public function photoAlbum(e:MouseEvent):void{
			blipSound.play();
			map.visible = false;
			auckSpot.visible = false;
			wellSpot.visible = false;
			christSpot.visible = false;
			nameTxt.visible = false;
			backBtn.visible = false;
			photoBtn.visible = false;
			lock.visible = false;
			loopBtn.gotoAndStop(1);
			loopBtn.visible = false;
			photo.visible = true;
			photoLabel.visible = true;
			skyLBtn.visible = true;
			skyRBtn.visible = true
			phoToMapBtn.visible = true;
			
			if (showSky && skyLBtn.visible == true){
				photoLabel.gotoAndPlay(16);
				skyTower.visible = true;
				skyTower.gotoAndPlay(1);
			}
			if (!showSky && skyLBtn.visible == true){
				photoLabel.gotoAndPlay(1);
			}
		}
		
		public function allCities(e:MouseEvent):void{
			blipSound.play();
			backBtn.visible = false;
			photoBtn.visible = false;
			lock.visible = false;
			loopBtn.gotoAndStop(1);
			loopBtn.visible = false;
			auckSpot.visible = false;
			wellSpot.visible = false;
			christSpot.visible = false;
			nameTxt.visible = false;
			map.gotoAndPlay(62);
			all.visible = true;
			all.gotoAndPlay(1);
			allToMapBtn.visible = true;
		}
		
		public function goToSkyF(e:MouseEvent):void{
			blipSound.play();
			chalice.visible = false
			chalLBtn.visible = false;
			chalRBtn.visible = false;
			photo.gotoAndPlay(1);
			photoLabel.visible = false;
			skyLBtn.visible = true;
			skyRBtn.visible = true;
		}
		
		public function goToSkyB(e:MouseEvent):void{
			blipSound.play();
			parliament.visible = false
			parLBtn.visible = false;
			parRBtn.visible = false;
			photo.gotoAndPlay(11);
			photoLabel.visible = false;
			skyLBtn.visible = true;
			skyRBtn.visible = true;
		}
		
		public function goToParF(e:MouseEvent):void{
			blipSound.play();
			skyTower.visible = false
			skyLBtn.visible = false;
			skyRBtn.visible = false;
			photo.gotoAndPlay(1);
			photoLabel.visible = false;
			parLBtn.visible = true;
			parRBtn.visible = true;
			
		}
		
		public function goToParB(e:MouseEvent):void{
			blipSound.play();
			beehive.visible = false
			beeLBtn.visible = false;
			beeRBtn.visible = false;
			photo.gotoAndPlay(11);
			photoLabel.visible = false;
			parLBtn.visible = true;
			parRBtn.visible = true;
		}
		
		public function goToBeeF(e:MouseEvent):void{
			blipSound.play();
			parliament.visible = false
			parLBtn.visible = false;
			parRBtn.visible = false;
			photo.gotoAndPlay(1);
			photoLabel.visible = false;
			beeLBtn.visible = true;
			beeRBtn.visible = true;
		}
		
		public function goToBeeB(e:MouseEvent):void{
			blipSound.play();
			cathedral.visible = false
			cathLBtn.visible = false;
			cathRBtn.visible = false;
			photo.gotoAndPlay(11);
			photoLabel.visible = false;
			beeLBtn.visible = true;
			beeRBtn.visible = true;
		}
		
		public function goToCathF(e:MouseEvent):void{
			blipSound.play();
			beehive.visible = false
			beeLBtn.visible = false;
			beeRBtn.visible = false;
			photo.gotoAndPlay(1);
			photoLabel.visible = false;
			cathLBtn.visible = true;
			cathRBtn.visible = true;
		}
		
		public function goToCathB(e:MouseEvent):void{
			blipSound.play();
			chalice.visible = false
			chalLBtn.visible = false;
			chalRBtn.visible = false;
			photo.gotoAndPlay(11);
			photoLabel.visible = false;
			cathLBtn.visible = true;
			cathRBtn.visible = true;
		}
		
		public function goToChalF(e:MouseEvent):void{
			blipSound.play();
			cathedral.visible = false
			cathLBtn.visible = false;
			cathRBtn.visible = false;
			photo.gotoAndPlay(1);
			photoLabel.visible = false;
			chalLBtn.visible = true;
			chalRBtn.visible = true;
		}
		
		public function goToChalB(e:MouseEvent):void{
			blipSound.play();
			skyTower.visible = false
			skyLBtn.visible = false;
			skyRBtn.visible = false;
			photo.gotoAndPlay(11);
			photoLabel.visible = false;
			chalLBtn.visible = true;
			chalRBtn.visible = true;
		}
		
		public function phoToMap(e:MouseEvent):void{
			blipSound.play();
			map.visible = true;
			auckSpot.visible = true;
			wellSpot.visible = true;
			christSpot.visible = true;
			nameTxt.visible = true;
			backBtn.visible = true;
			photoBtn.visible = true;
			if (showAll){
				loopBtn.visible = true;
				loopBtn.gotoAndPlay(1);
			}else{
				lock.visible = true;
			}
			photo.visible = false;
			photoLabel.gotoAndStop(1);
			photoLabel.visible = false;
			skyTower.visible = false;
			parliament.visible = false;
			beehive.visible = false;
			cathedral.visible = false;
			chalice.visible = false;
			skyLBtn.visible = false;
			skyRBtn.visible = false;
			parLBtn.visible = false;
			parRBtn.visible = false;
			beeLBtn.visible = false;
			beeRBtn.visible = false;
			cathLBtn.visible = false;
			cathRBtn.visible = false;
			chalLBtn.visible = false;
			chalRBtn.visible = false;
			phoToMapBtn.visible = false;
		}
		
		public function allToMap(e:MouseEvent):void{
			blipSound.play();
			all.gotoAndStop(1);
			showMap = true;
			allToMapBtn.visible = false;
		}
		
		public function auckStage(e:MouseEvent):void{
			blipSound.play();
			backBtn.visible = false;
			photoBtn.visible = false;
			lock.visible = false;
			loopBtn.gotoAndStop(1);
			loopBtn.visible = false;
			auckSpot.visible = false;
			wellSpot.visible = false;
			christSpot.visible = false;
			nameTxt.visible = false;
			map.gotoAndPlay(62);
			showAuck = true;
		}
		
		public function wellStage(e:MouseEvent):void{
			blipSound.play();
			backBtn.visible = false;
			photoBtn.visible = false;
			lock.visible = false;
			loopBtn.gotoAndStop(1);
			loopBtn.visible = false;
			auckSpot.visible = false;
			wellSpot.visible = false;
			christSpot.visible = false;
			nameTxt.visible = false;
			map.gotoAndPlay(62);
			showWell = true;
		}
		
		public function christStage(e:MouseEvent):void{
			blipSound.play();
			backBtn.visible = false;
			photoBtn.visible = false;
			lock.visible = false;
			loopBtn.gotoAndStop(1);
			loopBtn.visible = false;
			auckSpot.visible = false;
			wellSpot.visible = false;
			christSpot.visible = false;
			nameTxt.visible = false;
			map.gotoAndPlay(62);
			showChrist = true;
		}
		
		public function auckTxt(e:MouseEvent):void{
			nameTxt.gotoAndPlay(2)
		}
		
		public function wellTxt(e:MouseEvent):void{
			nameTxt.gotoAndPlay(3)
		}
		
		public function christTxt(e:MouseEvent):void{
			nameTxt.gotoAndPlay(4)
		}
		
		public function NZTxt(e:MouseEvent):void{
			nameTxt.gotoAndPlay(1)
		}
		
		public function creditsOver(e:MouseEvent):void{
			credits.gotoAndPlay(2)
		}
		
		public function creditsOut(e:MouseEvent):void{
			credits.gotoAndPlay(1)
		}
				
// main function
		public function playGame(e:Event){
			if (showTitle){
				showTitle = false;
				Title.visible = true;
				Title.gotoAndPlay(1);
			}
			
			if (Title.currentFrame == 40){
				startBtn.visible = true;
				credits.visible = true;
			}
			
			if (Title.currentFrame == Title.totalFrames){
				showBG = true;
				Title.gotoAndStop(1);
				Title.visible = false;
			}
			
			if (showBG){
				showBG = false;
				BG.visible = true;
				BG.gotoAndPlay(1);
			}
			
			if (BG.currentFrame == 19){
				showMap = true;
			}
			
			if (BG.currentFrame == BG.totalFrames){
				showTitle = true;
				BG.gotoAndStop(1);
				BG.visible = false;
			}
			
			if (showMap){
				showMap = false;
				map.visible = true;
				map.gotoAndPlay(1);
			}
			
			if (map.currentFrame == 60){
				showSpot = true;
			}
			
			if (map.currentFrame == map.totalFrames){
				map.gotoAndStop(1);
				map.visible = false;
			}
			
			if (showSpot){
				showSpot = false;
				auckSpot.visible = true;
				wellSpot.visible = true;
				christSpot.visible = true;
				nameTxt.visible = true;
				backBtn.visible = true;
				photoBtn.visible = true;
				if (showAll){
					loopBtn.visible = true;
					loopBtn.gotoAndPlay(1);
				}else{
					lock.visible = true;
				}
			}
			
			if (showAuck){
				showAuck = false;
				auck.visible = true;
				auck.gotoAndPlay(1);
			}
			
			if (auck.currentFrame == 420){
				shotSound.play();
				shot.visible = true;
				shot.gotoAndPlay(1);
				showSky = true;
			}
			
			if (auck.currentFrame == 439){
				mapBtn.visible = true;
			}
			
			if (auck.currentFrame == auck.totalFrames){
				showMap = true;
				auck.gotoAndStop(1);
				auck.visible = false;
			}
			
			if (showWell){
				showWell = false;
				well.visible = true;
				well.gotoAndPlay(1);
			}
			
			if (well.currentFrame == 420){
				shotSound.play();
				shot.visible = true;
				shot.gotoAndPlay(1);
				showPar = true;
				showBee = true;
			}
			
			if (well.currentFrame == 439){
				mapBtn.visible = true;
			}
			
			if (well.currentFrame == well.totalFrames){
				showMap = true;
				well.gotoAndStop(1);
				well.visible = false;
			}
			
			if (showChrist){
				showChrist = false;
				christ.visible = true;
				christ.gotoAndPlay(1);
			}
			
			if (christ.currentFrame == 420){
				shotSound.play();
				shot.visible = true;
				shot.gotoAndPlay(1);
				showCath = true;
				showChal = true;
			}
			
			if (christ.currentFrame == 439){
				mapBtn.visible = true;
			}
			
			if (christ.currentFrame == christ.totalFrames){
				showMap = true;
				christ.gotoAndStop(1);
				christ.visible = false;
			}
			
			if (shot.currentFrame == shot.totalFrames){
				shot.gotoAndStop(1);
				shot.visible = false;
			}
			
			if (photo.currentFrame == 9 || photo.currentFrame == 19){
				if (skyLBtn.visible == true){
					if (showSky){
						skyTower.visible = true;
						skyTower.gotoAndPlay(1);
						photoLabel.visible = true;
						photoLabel.gotoAndPlay(16);
					}else{
						photoLabel.visible = true;
						photoLabel.gotoAndPlay(1);
					}
				}
				if (parLBtn.visible == true){
					if (showPar){
						parliament.visible = true;
						parliament.gotoAndPlay(1);
						photoLabel.visible = true;
						photoLabel.gotoAndPlay(17);
					}else{
						photoLabel.visible = true;
						photoLabel.gotoAndPlay(1);
					}
				}
				if (beeLBtn.visible == true){
					if (showBee){
						beehive.visible = true;
						beehive.gotoAndPlay(1);
						photoLabel.visible = true;
						photoLabel.gotoAndPlay(18);
					}else{
						photoLabel.visible = true;
						photoLabel.gotoAndPlay(1);
					}
				}
				if (cathLBtn.visible == true){
					if (showCath){
						cathedral.visible = true;
						cathedral.gotoAndPlay(1);
						photoLabel.visible = true;
						photoLabel.gotoAndPlay(19);
					}else{
						photoLabel.visible = true;
						photoLabel.gotoAndPlay(1);
					}
				}
				if (chalLBtn.visible == true){
					if (showChal){
						chalice.visible = true;
						chalice.gotoAndPlay(1);
						photoLabel.visible = true;
						photoLabel.gotoAndPlay(20);
					}else{
						photoLabel.visible = true;
						photoLabel.gotoAndPlay(1);
					}
				}
			}
			
			if (showSky && showPar && showBee && showCath && showChal){
				showAll = true;
			}
		}	

	}
	
}
