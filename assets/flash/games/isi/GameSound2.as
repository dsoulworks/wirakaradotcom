﻿package{
	import flash.events.ProgressEvent;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;

	public class GameSound2{

		public var sound:Sound = new Sound();
		public var channel:SoundChannel = new SoundChannel();
		public var volumeControl:SoundTransform = new SoundTransform();
		public var volume:Number = 1;
		public var loaded:Boolean = false;
		public var queued:Boolean = false;

		public function GameSound2(url:String, vol = false){
			if(vol) volume = vol;
			sound.load(new URLRequest(url));

			sound.addEventListener(Event.COMPLETE, init);
		}

		public function init(e:Event){
			loaded = true;
			if(queued) play();
		}

		public function play(){
			if(!loaded){
				queued = true;
			}else{
				volumeControl.volume = volume;
				channel = sound.play(0,int.MAX_VALUE);
				channel.soundTransform = volumeControl;
			}
		}
		
		public function stop(){
			channel.stop();
		
		}
	}
}