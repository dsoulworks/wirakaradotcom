package {
	import flash.display.Sprite;
	import flash.events.Event;
	import com.nicksheffield.numberTools.*;

	public class Star extends Sprite{

		private var speed:Number;
		private var placement:Boolean;

		public function Star(prePlace:Boolean = false){
			// Constructor
			this.addEventListener(Event.ENTER_FRAME, update);
			this.addEventListener(Event.ADDED_TO_STAGE, init);

			speed = numberTools.getRandom(0.5, 2);
			placement = prePlace;
		}

		public function init(e:Event){
			x = placement ? numberTools.getRandom(0, stage.stageWidth) : stage.stageWidth + width;
			y = numberTools.getRandom(0, stage.stageHeight);
			width = height = speed * 1.7;
		}

		public function update(e:Event){
			x -= speed;

			if(x < 0){
				this.removeEventListener(Event.ENTER_FRAME, update);
				parent.removeChild(this);
			}
		}
	}

}