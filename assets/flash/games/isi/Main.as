﻿package  {
	import flash.display.MovieClip;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.events.MouseEvent;
	import flash.events.Event;
	
	import flash.media.Sound;
	import flash.media.SoundChannel;
	//import flash.utils.getQualifiedClassName;

	//import com.coreyoneil.collision.*;

	import Enemy;
	import Player;
	import Bullet;
	import Star;
	import GameSound;
	import HighScores;

	public class Main extends MovieClip{

		public var shootTimer:Timer = new Timer(100);
		public var enemySpawner:Timer = new Timer(500);
		public var starSpawner:Timer = new Timer(50);
		public var player:Player = new Player();
		public var playhit:MovieClip = new mcPlayerx();
		public var bound:MovieClip = new Bound();
		public var replay:MovieClip = new Replay();
		public var block:MovieClip = new Block();
		public var startGame:MovieClip = new mcStart();
		public var bullets:Array = new Array();
		public var enemies:Array = new Array();
		//public var laser:GameSound = new GameSound('laser.mp3', .5);
		//public var boom:GameSound = new GameSound('boom.mp3', .5);
		//public var pass:GameSound = new GameSound('pass.mp3', .5);
		//public var ohyeah:GameSound = new GameSound('ahyeah.mp3', 1);
		//public var heyjude:GameSound2 = new GameSound2('she.mp3', 1);
		public var highScores:HighScores = new HighScores('shooter');
		public var lives:int = 6;
		public var kills:int = 0;
		public var doom = false;
		
		public var heyjude:Sound = new She();
		public var myChannel1:SoundChannel = new SoundChannel();

		public var laser:Sound = new Laser();
		public var boom:Sound = new Boom();
		public var ohyeah:Sound = new Ahyeah();

		public var starfield:MovieClip = new MovieClip();
		public var stats_mc:mcStats = new mcStats();
		public var HighScoreBoard:mcGameOver = new mcGameOver();

		public function Main(){ // Constructor
		
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function init(e:Event){
			stage.quality = "HIGH";
			
			stage.frameRate = 60;
			
			stage.addChild(startGame);
			startGame.x = 450;
			startGame.y = 275;
			
			//heyjude.play();
			//myChannel1 =heyjude.play(0,int.MAX_VALUE);

			stage.addChild(starfield);
			stage.addChild(block);
			stage.addChild(player);
			stage.addChild(playhit);
			stage.addChild(bound);
			stage.addChild(stats_mc);
			
			stats_mc.x = 446;
			stats_mc.y = 20;
			
			playhit.x = player.x;
			playhit.y = player.y;
			
			bound.x = player.x;
			bound.y = player.y;
			
			block.x = 0;
			block.y = stage.stageHeight/2;
			
			block.visible = false;
			
			stats_mc.lives_txt.text = 'immunity: ' + lives;
			
			startGame.buttonMode = true;
			replay.buttonMode = true;

			stage.addEventListener(Event.ENTER_FRAME, update);
			
			startGame.addEventListener(MouseEvent.CLICK, playGame);
			
			replay.addEventListener(MouseEvent.CLICK, playAgain);

			enemySpawner.addEventListener(TimerEvent.TIMER, spawn);
			//enemySpawner.start();

			shootTimer.addEventListener(TimerEvent.TIMER, shoot);
			starSpawner.addEventListener(TimerEvent.TIMER, star);

			//starSpawner.start();
			
			stage.addEventListener(MouseEvent.MOUSE_DOWN, handleShoot);
			stage.addEventListener(MouseEvent.MOUSE_UP, handleShoot);

			for(var i:int=0;i<50;i++){
				var star:Star = new Star(true);
				starfield.addChild(star);
			}
		}
		
		public function playGame(e:MouseEvent){
			startGame.visible = false;
			myChannel1 =heyjude.play(0,int.MAX_VALUE);
			enemySpawner.start();
			starSpawner.start();
		}

		public function handleShoot(e:MouseEvent){
			if(e.type == MouseEvent.MOUSE_DOWN){
				shootTimer.start();
				shoot();
			}else{
				shootTimer.stop();
			}
		}

		public function spawn(e:TimerEvent){
			var enemy:Enemy = new Enemy();
			stage.addChild(enemy);
			enemies.push(enemy);
		}

		public function shoot(e = false){
			var bullet:Bullet = new Bullet(player.x, player.y);
			stage.addChild(bullet);
			bullets.push(bullet);
			bullet.rotation = player.rotation;

			laser.play();
		}

		public function star(e:TimerEvent){
			var star:Star = new Star();
			starfield.addChild(star);
		}
		
		public function playAgain(e:MouseEvent){
			block.visible = false;
			stats_mc.visible = true;;
			doom = false;
			lives = 6;
			kills = 0;
			
			enemySpawner.start();
			starSpawner.start();
			//heyjude.play();
			myChannel1 =heyjude.play(0,int.MAX_VALUE);
			
			player.gotoAndPlay(1);
			playhit.gotoAndPlay(1);
			
			lives = 6;
			kills = 0;
			
			stats_mc.lives_txt.text = 'immunity: ' + lives;
			stats_mc.kills_txt.text = 'Kills: ' + kills;
			
			stage.removeChild(HighScoreBoard);
			stage.removeChild(replay);
		}

		public function update(e:Event){
			playhit.x = player.x;
			playhit.y = player.y;
			
			bound.x = player.x;
			bound.y = player.y;
			
			playhit.rotation = player.rotation;
			
			for(var k:int=0;k<enemies.length;k++){
				var enemyx = enemies.shift();
				
				if(bound.hitTestObject(enemyx)){
					enemyx.y += (player.y - enemyx.y) * .01;
				}

				if(player.hitTestObject(enemyx)){
					if(enemyx.parent != null) enemyx.parent.removeChild(enemyx);
					playhit.nextFrame();
					
					lives--;
					//trace('Live: ' + lives);
					stats_mc.lives_txt.text = 'immunity: ' + lives;
					
					if (lives == 0){
						doom = true;
						block.visible = true;
						player.nextFrame();
						ohyeah.play();
						gameOver();
					}
				}else{
					enemies.push(enemyx);
				}
			}
			
			for(var t:int=0;t<enemies.length;t++){
				var enemyt = enemies.shift();
				if(block.hitTestObject(enemyt) && doom){
					if(enemyt.parent != null) enemyt.parent.removeChild(enemyt);
				}else{
					enemies.push(enemyt);
				}
			}
			
			for(var i:int=0;i<bullets.length;i++){
				var bullet = bullets.shift();
				
				if (bullet.x < 0 || bullet.x > 900 || bullet.y < 0 || bullet.y > 550){
					if(bullet.parent != null) bullet.parent.removeChild(bullet);
				}

				for(var j:int=0;j<enemies.length;j++){
					var enemy = enemies.shift();

					if(bullet.hitTestObject(enemy)){
						if(bullet.parent != null) bullet.parent.removeChild(bullet);
						if(enemy.parent != null) enemy.parent.removeChild(enemy);
						boom.play();
						kills++;
						stats_mc.kills_txt.text = 'Kills: ' + kills;
					}else{
						enemies.push(enemy);
					}
				}
				if(bullet.parent != null) bullets.push(bullet);
			}
		}
		
		public function gameOver(){
			trace('GAME OVER');
			stats_mc.visible = false;
			enemySpawner.stop();
			starSpawner.stop();
			//heyjude.stop();
			myChannel1.stop();
			
			stage.addChild(HighScoreBoard);
			stage.addChild(replay);
			
			//replay.buttonMode = true;
			
			HighScoreBoard.x = stage.stageWidth/2 - HighScoreBoard.width/2;
			HighScoreBoard.y = stage.stageHeight/2 - HighScoreBoard.height/2;
			
			replay.x = stage.stageWidth/2;
			replay.y = HighScoreBoard.y + 200;
			
			HighScoreBoard.previous_txt.text = highScores.getScore();
			
			highScores.saveScore(kills);
			
			HighScoreBoard.current_txt.text = String(kills);
			
			HighScoreBoard.new_txt.text = highScores.getScore();
			
		}
	}
}