﻿package  {
	import flash.display.MovieClip;
	import flash.events.Event;
	import com.nicksheffield.numberTools.*;
	//import flash.utils.getQualifiedClassName;
	import flash.utils.Timer;
	import com.greensock.TimelineLite;
	
	public class Enemy extends MovieClip {

		public var speed:int = -2;
		public var turn:Boolean = true;

		public function Enemy() { // Constructor
			addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.ENTER_FRAME, update);
		}

		public function init(e:Event){
			this.x = stage.stageWidth + this.width;
			this.y = numberTools.getRandom(this.height / 2, stage.stageHeight - this.height / 2);
		}

		public function update(e:Event){
			//x -= speed;
			x += speed;
			
			if (x > 890 && turn){
				turn = false;
				this.gotoAndPlay(1);
				speed = -2;
			}
			
			if (x < 0 && !turn){
				turn = true;
				this.gotoAndPlay(3);
				speed = 2;
				/*this.removeEventListener(Event.ENTER_FRAME, update);
				parent.removeChild(this);*/
			}
		}
		
		/*public function RIP(e:TimerEvent){
			//this.removeEventListener(TimerEvent.TIMER,die);
			parent.removeChild(this);
		}*/
	}
	
}
