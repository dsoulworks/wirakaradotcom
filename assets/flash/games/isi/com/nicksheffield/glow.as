function glow(mc:MovieClip, color:uint = 0xFFFFFF, strength:int = 10, quality:int = 1){
	var glow:GlowFilter = new GlowFilter();
	glow.blurX = glow.blurY = strength;
	glow.quality = quality;
	glow.color = color;
	
	mc.filters = [glow];
}

function color(mc:MovieClip, color){
	var colorTransform:ColorTransform = new ColorTransform();
		colorTransform.color = color;
		mc.transform.colorTransform = colorTransform;
}