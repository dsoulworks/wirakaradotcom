﻿package  {
	import flash.display.MovieClip;
	import flash.events.Event;
	import com.nicksheffield.angleTools.*;
	
	public class Bullet extends MovieClip {
		
		public var speed:int = 3;
		
		public var leftLimit:int;
		public var upLimit:int;
		public var rightLimit:int;
		public var downLimit:int;

		public function Bullet(x:int = 0, y:int = 0) { // Constructor
			
			this.addEventListener(Event.ENTER_FRAME, update);
			this.addEventListener(Event.ADDED_TO_STAGE, added);
			
			this.x = x;
			this.y = y;
		}
		
		public function added(e:Event){
			leftLimit = 0;
			upLimit = 0;
			rightLimit = stage.stageWidth;
			downLimit = stage.stageHeight;
		}
		
		public function update(e:Event){
			angleTools.moveOnRotation(this);
			
			/*if(this.x < leftLimit || this.x > rightLimit ||
			   this.y < upLimit || this.y > downLimit){
				//destroy();
			}*/
			/*if (x < 0 || x > 900 || y < 0 || y > 550){
				this.removeEventListener(Event.ENTER_FRAME, update);
				parent.removeChild(this);
				
				
			}*/
		}
		
		/*public function destroy(){
			stage.removeChild(this);
			this.removeEventListener(Event.ENTER_FRAME, update);
		}*/

	}
	
}
